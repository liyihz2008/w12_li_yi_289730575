目录结构：
news:
    |----- home
        |----- migrations 数据库版本管理
        |----- model        实体数据模型
        |----- services      数据库服务类
        |----- utils            工具类
        |----- view            视图类
    |----- news
    |----- static
        |----- ckeditor5    HTML编辑器
        |----- css                CSS样式表文件
        |----- images        图片文件
        |----- js                 JS文件
        |----- sy-img         图片文件
        |----- *.html          网页HTML文件
    |----- templates
        |----- article_index.html      查看一篇新闻的首页
        |----- index.html                  网站首页


二、数据库初始化命令：
       1、检查news.setting.py里参数INSTALLED_APPS是否有"home"这个app
       2、执行命令
            python manage.py makemigrations
            python manage.py migrate
            -----------------------------------------------------------------------------------------
            D:\MyWorkHome\PythonHome\DjangoApps\news>python manage.py migrate
            Operations to perform:
              Apply all migrations: admin, auth, contenttypes, home, sessions
            Running migrations:
              Applying home.0001_initial... OK
            -----------------------------------------------------------------------------------------

三、后台管理：
       命令：python manage.py createsuperuser
       地址：http://127.0.0.1:8000/admin
       用户：liyi     密码：!QAZXSW@

其他参考：
一、Django Ajax CSRF 认证：https://code.ziqiangxuetang.com/django/django-csrf.html（目前实现先跳过了csrf）
二、Django序列化问题，现在只是在实体内里写了简单的序列化方法
       https://github.com/bluedazzle/django-simple-serializer  这个包感觉更方便
       依赖包future： pip install future


网站访问：http://127.0.0.1:8000
注册用户： liyi@zoeland.net     密码：1
也可以自己注册

目前因为时间的关系 ，只实现了登录，注册，投稿和展现列表，琢磨一些类似数据库事务管理，前后来分离，序列化等问题
其他的分类管理，标签管理以及评论管理都没有处理，以后有时间再说
