# -*- coding: utf-8 -*-
"""news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.urls import path

from home.view.article import save_article, filter_articles, view_article
from home.view.index import show_index_page, show_page, show_article_page
from home.view.login import login_user, logout, login
from home.view.user import user_operator, regist_new_user
from home.view.user import users_filter

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', show_index_page),
	path('show_page/<str:name>', show_page),
	path('login', login),
	path('logout', logout),
	path('login_user', login_user),
	path('home/user/regist', regist_new_user),
	path('home/users', users_filter),
	path('home/user/<str:id>/', user_operator),
	path('home/article', save_article),
	path('home/articles', filter_articles),
	path('article/view/<str:id>/', show_article_page),
	path('home/article/view/<str:article_id>/', view_article),
]

# ... the rest of your URLconf goes here ...
urlpatterns += staticfiles_urlpatterns()
