 **目录结构：** <br/>
&nbsp;|----- home<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- migrations 数据库版本管理<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- model        实体数据模型<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- services      数据库服务类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- utils            工具类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- view            视图类<br/>
&nbsp;|----- news<br/>
&nbsp;|----- static<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- ckeditor5    HTML编辑器<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- css                CSS样式表文件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- images        图片文件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- js                 JS文件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- sy-img         图片文件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- *.html          网页HTML文件<br/>
&nbsp;|----- templates<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- article_index.html      查看一篇新闻的首页<br/>
&nbsp;&nbsp;&nbsp;&nbsp;|----- index.html                  网站首页<br/>
<br/>

 **二、数据库初始化命令：** <br/>
       1、检查news.setting.py里参数INSTALLED_APPS是否有"home"这个app<br/>
       2、执行命令<br/>
            python manage.py makemigrations<br/>
            python manage.py migrate<br/>
            -----------------------------------------------------------------------------------------<br/>
            D:\MyWorkHome\PythonHome\DjangoApps\news>python manage.py migrate<br/>
            Operations to perform:<br/>
              Apply all migrations: admin, auth, contenttypes, home, sessions<br/>
            Running migrations:<br/>
              Applying home.0001_initial... OK<br/>
            -----------------------------------------------------------------------------------------<br/>
<br/>

 **三、后台管理：** <br/>
       命令：python manage.py createsuperuser<br/>
       地址：http://127.0.0.1:8000/admin<br/>
       用户：liyi     密码：!QAZXSW@<br/>
<br/>
 **其他参考：** <br/>
一、Django Ajax CSRF 认证：https://code.ziqiangxuetang.com/django/django-csrf.html（目前实现先跳过了csrf）<br/>
二、Django序列化问题，现在只是在实体内里写了简单的序列化方法<br/>
       https://github.com/bluedazzle/django-simple-serializer  这个包感觉更方便<br/>
       依赖包future： pip install future<br/>


网站访问：http://127.0.0.1:8000<br/>
注册用户： liyi@zoeland.net     密码：1<br/>
也可以自己注册<br/>
<br/>
目前因为时间的关系 ，只实现了登录，注册，投稿和展现列表，琢磨一些类似数据库事务管理，前后来分离，序列化等问题<br/>
其他的分类管理，标签管理以及评论管理都没有处理，以后有时间再说<br/>
