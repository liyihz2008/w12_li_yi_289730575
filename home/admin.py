# -*- coding: utf-8 -*-
from django.contrib import admin

from home.model import User_Info, Article_Info, Article_Pic, Article_Detail, Category_Info, Comment_Info, \
	Comment_Detail, Label_Info, Article_Label


class User_Info_Admin(admin.ModelAdmin):
	list_display = ("id", "name", "email", "nick_name", "is_admin", "article_count", "create_time", "update_time")


class Article_Info_Admin(admin.ModelAdmin):
	list_display = (
		"id", "category_id", "title", "author", "summary", "comment_count", "view_count", "original", "pic_id",
		"create_time", "update_time")


class Article_Pic_Admin(admin.ModelAdmin):
	list_display = ("id", "base64Content", "create_time", "update_time")


class Article_Detail_Admin(admin.ModelAdmin):
	list_display = ("id", "content", "create_time", "update_time")


class Category_Info_Admin(admin.ModelAdmin):
	list_display = ("id", "parent_id", "name", "level", "article_count", "create_time", "update_time")


class Comment_Info_Admin(admin.ModelAdmin):
	list_display = ("id", "article_id", "title", "author", "reference_id", "create_time", "update_time")


class Comment_Detail_Admin(admin.ModelAdmin):
	list_display = ("id", "content", "create_time", "update_time")


class Label_Info_Admin(admin.ModelAdmin):
	list_display = ("id", "name", "create_time", "update_time")


class Article_Label_Admin(admin.ModelAdmin):
	list_display = ("id", "article_id", "label_id")


"""向Django管理控制服务注册模型类"""
admin.site.register(User_Info, User_Info_Admin)
admin.site.register(Article_Info, Article_Info_Admin)
admin.site.register(Article_Pic, Article_Pic_Admin)
admin.site.register(Article_Detail, Article_Detail_Admin)
admin.site.register(Category_Info, Category_Info_Admin)
admin.site.register(Comment_Info, Comment_Info_Admin)
admin.site.register(Comment_Detail, Comment_Detail_Admin)
admin.site.register(Label_Info, Label_Info_Admin)
admin.site.register(Article_Label, Article_Label_Admin)
