# -*- coding: utf-8 -*-
from django.shortcuts import render


def show_page(request, name="index.html"):
	"""
	静态页面展示服务
	:param request:
	:param page_name:
	:return:
	"""
	return render(request, name)


def show_index_page(request):
	"""
	首页展示服务
	:param request:
	:return:
	"""
	return render(request, "index.html", {"type": "index"})


def show_article_page(request, id):
	"""
	跳转到展示指定新闻的页面
	:param request:
	:return:
	"""
	return render(request, "article_index.html", {"type": "article", "article_id": id})
