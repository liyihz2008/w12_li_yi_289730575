# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from home.model import Category_Info
from home.model.category_info import Category_Info_Serializer
from home.services.category_info_service import Category_Info_Service
from home.utils.post_parameter_getter import ParamGetter


@csrf_exempt
def save_category(request):
	"""
		文章信息保存服务
		:param request:
		:return:
		"""
	if request.method == "POST":
		category_Info_Service = Category_Info_Service()
		category_Info = Category_Info()
		category_Info.id = ParamGetter.getParam(request, "id")
		category_Info.parent_id = ParamGetter.getParam(request, "parent_id")
		category_Info.name = ParamGetter.getParam(request, "name")
		category_Info.level = ParamGetter.getParam(request, "level")
		category_Info.article_count = ParamGetter.getParam(request, "article_count", 0)
		category_Info.create_time = ParamGetter.getParam(request, "create_time")
		try:
			# 持久化到数据库中
			category_Info_Service.save_category(category_Info)
			result = {"status": "success", "message": "新闻保存成功！"}
		except Exception:
			result = {"status": "error", "message": "新闻信息保存时发生异常！"}
			import traceback
			print(traceback.format_exc())
	return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def category_operator(request, category_id):
	"""
	分类信息查询服务：根据新闻D查询或者删除指定的分类信息
	:param category_id:
	:param request:
	:return:
	"""
	if request.method == "GET":
		category_info_service = Category_Info_Service()
		category_info = category_info_service.get_with_id(category_id)
		if category_info is not None:
			result = {"status": "success", "message": "分类信息查询成功！",
			          "user": json.dumps(Category_Info_Serializer.to_json_object(category_info), ensure_ascii=False)}
		else:
			result = {"status": "error", "message": "分类信息不存在！ID=" + category_id}
	if request.method == "DELETE":
		category_Info_Service = Category_Info_Service()
		if category_Info_Service.delete_category(category_id):
			result = {"status": "success", "message": "分类信息删除成功！ID=" + category_id}
		else:
			result = {"status": "error", "message": "分类信息删除失败！ID=" + category_id}
	return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def filter_categorys(request):
	"""
		分类信息搜索查询服务：根据过滤条件搜索分类信息列表
		:param request:
		:return:
		"""
	if request.method == "PUT":
		category_Info_Service = Category_Info_Service()
		parent_id = ParamGetter.getParam(request, "parent_id")
		name = ParamGetter.getParam(request, "name")
		level = ParamGetter.getParam(request, "level")
		try:
			categorys = category_Info_Service.filter_category_info(parent_id, name, level)
			result = {"status": "success", "message": "分类信息查询成功！",
			          "user": json.dumps(Category_Info_Serializer.to_json_object(categorys), ensure_ascii=False)}
		except Exception as err:
			result = {"status": "error", "message": "分类信息查询时发生异常！"}
			import traceback
			print(traceback.format_exc())
		return HttpResponse(json.dumps(result), content_type="application/json")
