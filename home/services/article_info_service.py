# -*- coding: utf-8 -*-
import datetime
from django.db import transaction
from home.model import Article_Info, Article_Detail, Category_Info
from home.utils.array_utils import ListTools
from home.utils.string_utils import StringUtils

"""
文章信息管理服务类
"""


class Article_Info_Service:
	
	def save_article(self, article_info, article_detail):
		"""
		保存文章信息和文章内容
		:param article_info:
		:param article_content:
		:return:
		"""
		if article_info is None:
			return False
		# 使用事务保存文章信息和文章内容信息
		with transaction.atomic():
			article_info = self.save_article_info(article_info)
			article_detail = self.save_article_detail(article_info.id, article_detail)
		return article_info, article_detail
	
	def save_article_info(self, article_info):
		if StringUtils.isEmpty(article_info.id):
			article_info.set_new_id()
		# 查询文章是否已经存在
		entities = Article_Info.objects.filter(id=article_info.id)
		if ListTools.isNotEmpty(entities):
			entity = entities[0]
			# 文章信息已经存在，更新信息
			entity.category_id = article_info.category_id
			entity.title = article_info.title
			entity.author = article_info.author
			entity.summary = article_info.summary
			entity.comment_count = article_info.comment_count
			entity.view_count = article_info.view_count
			entity.original = article_info.original
			entity.pic_id = article_info.pic_id
			entity.create_time = article_info.create_time
			entity.update_time = datetime.datetime.now()
			entity.save()
		else:
			# 文章信息不存在
			if StringUtils.isEmpty(article_info.id):
				article_info.set_new_id()
			article_info.comment_count = 0
			article_info.view_count = 0
			article_info.create_time = datetime.datetime.now()
			article_info.update_time = datetime.datetime.now()
			Article_Info.objects.create(id=article_info.id, category_id=article_info.category_id,
			                            title=article_info.title,
			                            author=article_info.author, summary=article_info.summary,
			                            comment_count=article_info.comment_count,
			                            view_count=article_info.view_count, original=article_info.original,
			                            pic_id=article_info.pic_id, create_time=article_info.create_time,
			                            update_time=article_info.update_time)
			# category 文章分类文章数+1
			categorites = Category_Info.objects.filter(id=article_info.category_id).all()
			if ListTools.isNotEmpty(categorites):
				categorites[0].increase_article_count()
				categorites[0].save()
		return article_info
	
	def save_article_detail(self, article_id, article_detail):
		if StringUtils.isEmpty(article_id):
			raise Exception("article_id can not be empty!!!")
		
		article_detail.id = article_id
		# 查询文章内容是否已经存在
		entities = Article_Detail.objects.filter(id=article_id)
		if ListTools.isNotEmpty(entities):
			entity = entities[0]
			# 文章内容已经存在，更新信息
			entity.content = article_detail.content
			entity.create_time = article_detail.create_time
			entity.update_time = datetime.datetime.now()
			entity.save()
		else:
			# 文章内容信息不存在
			article_detail.create_time = datetime.datetime.now()
			article_detail.update_time = datetime.datetime.now()
			Article_Detail.objects.create(id=article_detail.id, content=article_detail.content,
			                              create_time=article_detail.create_time,
			                              update_time=article_detail.update_time)
		return article_detail
	
	def delete_article(self, article_id):
		"""
		根据ID删除指定的文章信息
		:param article_id:
		:return:
		"""
		if StringUtils.isEmpty(article_id):
			raise Exception("article_id can not be empty!")
		# 根据ID删除数据，事务管理
		with transaction.commit_on_success(self):
			article_info = Article_Info.objects.filter(id=article_id)
			if article_info is not None:
				article_info.delete()
				# category 文章分类文章数-1
				categorites = Category_Info.objects.filter(id=article_info.category_id).all()
				if ListTools.isNotEmpty(categorites):
					categorites[0].decrease_article_count()
					categorites[0].save()
			Article_Detail.objects.filter(id=article_id).delete()
	
	def get_with_id(self, article_id):
		if StringUtils.isEmpty(article_id):
			return None
		entities = Article_Info.objects.filter(id=article_id).all()
		if ListTools.isNotEmpty(entities):
			return entities[0]
		else:
			return None
	
	def get_content_with_id(self, article_id):
		if StringUtils.isEmpty(article_id):
			return None
		entities = Article_Detail.objects.filter(id=article_id).all()
		if ListTools.isNotEmpty(entities):
			return entities[0].content
		else:
			return None
	
	def filter_article_info(self, category_id, title, author, original, label):
		"""
		根据作者、分类以及标签过滤文章信息列表
		:param category_id:
		:param title:
		:param author:
		:param original:
		:param label:
		:return:
		"""
		conditions = {}
		if StringUtils.isNotEmpty(category_id):
			conditions["category_id"] = category_id
		if StringUtils.isNotEmpty(title):
			conditions["title__icontains"] = title
		if StringUtils.isNotEmpty(author):
			conditions["author"] = author
		if StringUtils.isNotEmpty(original):
			conditions["original"] = original
		if StringUtils.isNotEmpty(label):
			conditions["label"] = label
		return Article_Info.objects.filter(**conditions).all()
	
	def count_article_info(self, category_id, title, author, original, label):
		"""
		根据作者、分类以及标签过滤文章信息列表
		:param category_id:
		:param title:
		:param author:
		:param original:
		:param label:
		:return:
		"""
		conditions = {}
		if StringUtils.isNotEmpty(category_id):
			conditions["category_id"] = category_id
		if StringUtils.isNotEmpty(title):
			conditions["title__icontains"] = title
		if StringUtils.isNotEmpty(author):
			conditions["author"] = author
		if StringUtils.isNotEmpty(original):
			conditions["original"] = original
		if StringUtils.isNotEmpty(label):
			conditions["label"] = label
		return Article_Info.objects.filter(**conditions).count()
