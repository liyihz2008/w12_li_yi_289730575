# -*- coding: utf-8 -*-
import datetime
from django.db import transaction
from home.model import User_Info, Article_Info, Article_Detail, Category_Info
from home.utils.array_utils import ListTools
from home.utils.string_utils import StringUtils

"""
文章分类信息管理服务类
"""


class Category_Info_Service:
	
	def save_category(self, category_info):
		"""
		保存文章分类信息
		:param category_info:
		:return:
		"""
		if category_info is None:
			raise Exception("category info for save can not be None")
		
		if StringUtils.isEmpty(category_info.id):
			category_info.set_new_id()
		# 查询文章分类是否已经存在
		entities = Category_Info.objects.filter(id=category_info.id)
		if ListTools.isNotEmpty(entities):
			entity = entities[0]
			# 文章分类信息已经存在，更新分类信息
			entity.parent_id = category_info.parent_id
			entity.name = category_info.name
			entity.level = category_info.level
			entity.article_count = category_info.article_count
			entity.create_time = category_info.create_time
			entity.update_time = datetime.datetime.now()
			entity.save()
		else:
			# 文章分类信息不存在
			category_info.article_count = 0
			category_info.create_time = datetime.datetime.now()
			category_info.update_time = datetime.datetime.now()
			Category_Info.objects.create(id=category_info.id, parent_id=category_info.parent_id, name=category_info.name,
			                             level=category_info.level, article_count=category_info.article_count,
			                             create_time=category_info.create_time,
			                             update_time=category_info.update_time)
		return category_info
	
	def delete_category(self, category_id):
		"""
		根据ID删除指定的文章分类信息，分类下仍存在文章时报错，不会执行删除操作
		:param category_id:
		:return:
		"""
		if StringUtils.isEmpty(category_id):
			raise Exception("category_id can not be empty!")
		# 根据ID删除数据，事务管理
		exits_articles = Article_Info.objects.filter(category_id__exact=category_id).all()
		if not exits_articles:
			# 最后删除该分类信息
			Category_Info.objects.filter(id=category_id).delete()
		else:
			raise Exception("文章分类内仍存在文章信息，不允许删除！")
			
	def delete_category_force(self, category_id):
		"""
		根据ID删除指定的文章分类信息，分类下所有的文章全部都会删除
		:param category_id:
		:return:
		"""
		if StringUtils.isEmpty(category_id):
			raise Exception("category_id can not be empty!")
		# 根据ID删除数据，事务管理
		with transaction.commit_on_success(self):
			# 先删除所有的文章和文章内容信息
			# 删除一篇文章，该文章对应的作者的文章数量会减1
			article_entities = Article_Info.objects.filter(category_id=category_id).all()
			if ListTools.isNotEmpty(article_entities):
				for article_entity in article_entities:
					# 减少文章作者的文章计数
					user_entities = User_Info.objects.filter(id=article_entity.author).all()
					if ListTools.isNotEmpty(user_entities):
						for user_entity in user_entities:
							user_entity.decrease_article_count()
					# 删除文章内容信息
					Article_Detail.objects.filter(id=article_entity.id).delete()
					# 删除文章信息
					article_entity.delete()
			# 最后删除该分类信息
			Category_Info.objects.filter(id=category_id).delete()
			
	def filter_category_info(self, parent_id=None, name=None, level=None):
		"""
		根据条件过滤分类信息列表
		"""
		conditions = {}
		if StringUtils.isNotEmpty(parent_id):
			conditions["parent_id"] = parent_id
		if StringUtils.isNotEmpty(name):
			conditions["name__icontains"] = name
		if StringUtils.isNotEmpty(level):
			conditions["level"] = level
		return Category_Info.objects.filter(**conditions).all()

