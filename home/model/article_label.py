# -*- coding: utf-8 -*-
import uuid
from django.db import models

from home.utils.array_utils import ListTools
from home.utils.string_utils import StringUtils


class Article_Label(models.Model):
	"""
	文章标签关联信息表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# 文章ID
	article_id = models.CharField(max_length=64)
	# 标签ID
	label_id = models.CharField(max_length=64)
	
	def set_new_id(self, _id=None):
		"""
		为对象设置一个ID，如果传入的ID为空，则生成一个新的ID
		:param _id:
		:return:
		"""
		if _id is None or StringUtils.isEmpty(_id):
			self.id = Article_Label.create_id()
		else:
			self.id = _id
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())
	
	class Meta:
		app_label = 'home'
