# -*- coding: utf-8 -*-
import uuid
from django.db import models
from home.utils.array_utils import ListTools
from home.utils.string_utils import StringUtils


class User_Info(models.Model):
	"""
	用户信息表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# 姓名
	name = models.CharField(max_length=128)
	# 邮件地址
	email = models.CharField(max_length=64)
	# 昵称
	nick_name = models.CharField(max_length=128)
	# 登录密码
	user_pwd = models.CharField(max_length=128)
	# 是否管理员
	is_admin = models.BooleanField(default=False)
	# 文章数量
	article_count = models.IntegerField(default=0)
	# 创建时间
	create_time = models.DateTimeField()
	# 最后更新时间
	update_time = models.DateTimeField()
	
	def set_new_id(self, _id=None):
		"""
		为对象设置一个ID，如果传入的ID为空，则生成一个新的ID
		:param _id:
		:return:
		"""
		if _id is None or StringUtils.isEmpty(_id):
			self.id = User_Info.create_id()
		else:
			self.id = _id
	
	def increase_article_count(self, count=1):
		"""
		增加文章计数
		:param count:1
		:return:
		"""
		if self.article_count is None:
			self.article_count = 0
		self.article_count = self.article_count + count
	
	def decrease_article_count(self, count=1):
		"""
		减少文章计数
		:param count:1
		:return:
		"""
		if self.article_count is None:
			self.article_count = 0
		self.article_count = self.article_count - count
		if self.article_count < 0:
			self.article_count = 0
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())
	
	class Meta:
		app_label = 'home'


class User_Info_Serializer:
	@classmethod
	def to_json_object(cls, entity):
		keys = (
			"id", "name", "email", "nick_name", "user_pwd", "is_admin", "article_count", "create_time", "update_time")
		json_obj = {}
		for key in keys:
			if entity.__getattribute__(key) is not None:
				value_type = type(entity.__getattribute__(key)).__name__
				if "datetime" == value_type or "time" == value_type:
					json_obj[key] = entity.__getattribute__(key).strftime("%Y-%m-%d %H:%M:%S")
				else:
					json_obj[key] = entity.__getattribute__(key)
		return json_obj
	
	@classmethod
	def to_json_objects(cls, entities):
		json_objs = []
		if ListTools.isNotEmpty(entities):
			for entity in entities:
				json_obj = cls.to_json_object(entity)
				json_objs.append(json_obj)
		return json_objs
