# -*- coding: utf-8 -*-
from django.db import models


class Comment_Detail(models.Model):
	"""
	文章评论内容表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# 内容10MB
	content = models.TextField(max_length=10000000)
	# 创建时间
	create_time = models.DateTimeField()
	# 最后更新时间
	update_time = models.DateTimeField()

	class Meta:
		app_label = 'home'