# -*- coding: utf-8 -*-
from django.db import models


class Comment_Info(models.Model):
	"""
	文章评论信息表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# 文章ID
	article_id = models.CharField(max_length=64)
	# 标题
	title = models.CharField(max_length=128)
	# 作者
	author = models.CharField(max_length=64)
	# 引用评论ID
	reference_id = models.CharField(max_length=64, null=True)
	# 创建时间
	create_time = models.DateTimeField()
	# 最后更新时间
	update_time = models.DateTimeField()

	class Meta:
		app_label = 'home'