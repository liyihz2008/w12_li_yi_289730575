# -*- coding: utf-8 -*-
from django.db import models

from home.utils.array_utils import ListTools


class Article_Pic(models.Model):
	"""
	文章图片信息表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# base64编码内容，1MB
	base64Content = models.TextField(max_length=1000000)
	# 创建时间
	create_time = models.DateTimeField()
	# 最后更新时间
	update_time = models.DateTimeField()
	
	class Meta:
		app_label = 'home'


class Article_Pic_Serializer:
	@classmethod
	def to_json_object(cls, entity):
		keys = ("id", "base64Content", "create_time", "update_time")
		json_obj = {}
		for key in keys:
			if entity.__getattribute__(key) is not None:
				value_type = type(entity.__getattribute__(key)).__name__
				if "datetime" == value_type or "time" == value_type:
					json_obj[key] = entity.__getattribute__(key).strftime("%Y-%m-%d %H:%M:%S")
				else:
					json_obj[key] = entity.__getattribute__(key)
		return json_obj
	
	@classmethod
	def to_json_objects(cls, entities):
		json_objs = []
		if ListTools.isNotEmpty(entities):
			for entity in entities:
				json_obj = cls.to_json_object(entity)
				json_objs.append(json_obj)
		return json_objs
