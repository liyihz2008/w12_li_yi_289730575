# -*- coding: utf-8 -*-
import uuid
from django.db import models

from home.utils.array_utils import ListTools
from home.utils.string_utils import StringUtils


class Label_Info(models.Model):
	"""
	标签信息表
	"""
	# ID
	id = models.CharField(max_length=64, primary_key=True)
	# 名称
	name = models.CharField(max_length=128)
	# 创建时间
	create_time = models.DateTimeField()
	# 最后更新时间
	update_time = models.DateTimeField()
	
	def set_new_id(self, _id=None):
		"""
		为对象设置一个ID，如果传入的ID为空，则生成一个新的ID
		:param _id:
		:return:
		"""
		if _id is None or StringUtils.isEmpty(_id):
			self.id = Label_Info.create_id()
		else:
			self.id = _id
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())
	
	class Meta:
		app_label = 'home'


class Label_Info_Serializer:
	@classmethod
	def to_json_object(cls, entity):
		keys = (
			"id", "name", "create_time", "update_time")
		json_obj = {}
		for key in keys:
			if entity.__getattribute__(key) is not None:
				value_type = type(entity.__getattribute__(key)).__name__
				if "datetime" == value_type or "time" == value_type:
					json_obj[key] = entity.__getattribute__(key).strftime("%Y-%m-%d %H:%M:%S")
				else:
					json_obj[key] = entity.__getattribute__(key)
		return json_obj
	
	@classmethod
	def to_json_objects(cls, entities):
		json_objs = []
		if ListTools.isNotEmpty(entities):
			for entity in entities:
				json_obj = cls.to_json_object(entity)
				json_objs.append(json_obj)
		return json_objs