# -*- coding: utf-8 -*-
import json


class ParamGetter:
	
	@classmethod
	def getParam(cls, request, key, default_value=None):
		"""
		从请求中获取POST数据
		:param request:
		:param default_value:
		:param key:
		:return:
		"""
		if request.body is not None:
			received_json_data = json.loads(request.body)
			if key in received_json_data:
				return received_json_data[key]
		if default_value is not None:
			return default_value
		else:
			return None

