# -*- coding: utf-8 -*-


class StringUtils:
	
	@classmethod
	def isEmpty(cls, string=""):
		"""
		判断字符串是否为空
		:param string:
		:return:
		"""
		if string is None or len(string.strip()) == 0:
			return True
		else:
			return False
	
	@classmethod
	def isNotEmpty(cls, string=""):
		"""
		判断字符串是否非空
		:param string:
		:return:
		"""
		if string is not None and len(string.strip()) > 0:
			return True
		else:
			return False
