# -*- coding: utf-8 -*-
import datetime


class LoggerUtil:
	
	@classmethod
	def info(cls, message):
		"""
		记录日志，级别[INFO]
		:param message:
		:return:
		"""
		print(str(datetime.datetime.now()) + "  [INFO]  " + message)
	
	@classmethod
	def debug(cls, message):
		"""
		记录日志，级别[DEBUG]
		:param message:
		:return:
		"""
		print(str(datetime.datetime.now()) + "  [DEBUG]  " + message)
	
	@classmethod
	def warn(cls, message):
		"""
		记录日志，级别[WARN]
		:param message:
		:return:
		"""
		print(str(datetime.datetime.now()) + "  [WARN]  " + message)
	
	@classmethod
	def error(cls, message):
		"""
		记录日志，级别[ERROR]
		:param message:
		:return:
		"""
		print(str(datetime.datetime.now()) + "  [ERROR]  " + message)

