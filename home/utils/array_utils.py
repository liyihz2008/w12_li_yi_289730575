# -*- coding: utf-8 -*-
import numpy as npy
from home.utils.string_utils import StringUtils


class ListTools:
	
	@classmethod
	def isEmpty(cls, array=[]):
		"""
		判断数组是否为空
		:param array:
		:return:
		"""
		if array is None or len(array) == 0:
			return True
		else:
			return False
	
	@classmethod
	def isNotEmpty(cls, array=[]):
		"""
		判断数组是否非空
		:param array:
		:return:
		"""
		if array is not None and len(array) > 0:
			return True
		else:
			return False
	
	@classmethod
	def ndarray_to_string(cls, array=None):
		"""
		将一个numpy数组转换为字符串（方便持久化到数据库）
		:param array:
		:return:
		"""
		if cls.isNotEmpty(array) > 0:
			return str(array.tolist())
	
	@classmethod
	def string_to_ndarray(cls, string=None):
		"""
		将一个字符串还原为一个numpy数组
		:param string:
		:return:
		"""
		if StringUtils.isNotEmpty(string):
			return npy.array(eval(string))
